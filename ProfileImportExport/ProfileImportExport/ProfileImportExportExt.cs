﻿using KeePass.Plugins;
using KeePass.Util;
using KeePassLib.Cryptography.PasswordGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileImportExport
{
	public class ProfileImportExportExt : Plugin
	{
		private IPluginHost pluginHost = null;
		private ToolStripItemCollection tsMenu = null;
		private ToolStripSeparator tssPreProfileImportExport = null;
		private ToolStripMenuItem tsmiProfileImportExport = null;
		private ToolStripMenuItem tsmiProfileExport = null;
		private ToolStripMenuItem tsmiProfileImport = null;

		public override string UpdateUrl
		{
			get
			{
				return "http://derpyhooves.sytes.net/dev/kpplugins/version";
			}
		}

		public override bool Initialize(IPluginHost host)
		{
			this.pluginHost = host;

			this.tsMenu = this.pluginHost.MainWindow.ToolsMenu.DropDownItems;

			this.tssPreProfileImportExport = new ToolStripSeparator();
			this.tsMenu.Add(this.tssPreProfileImportExport);

			this.tsmiProfileImportExport = new ToolStripMenuItem("ProfileImportExport");
			this.tsMenu.Add(this.tsmiProfileImportExport);

			this.tsmiProfileExport = new ToolStripMenuItem("Export");
			this.tsmiProfileExport.Click += tsmiProfileExport_Click;
			this.tsmiProfileImportExport.DropDownItems.Add(this.tsmiProfileExport);

			this.tsmiProfileImport = new ToolStripMenuItem("Import");
			this.tsmiProfileImport.Click += tsmiProfileImport_Click;
			this.tsmiProfileImportExport.DropDownItems.Add(this.tsmiProfileImport);

			return true;
		}

		public override void Terminate()
		{
			this.tsmiProfileExport.Click -= tsmiProfileExport_Click;
			this.tsmiProfileImport.Click -= tsmiProfileImport_Click;
			this.tsMenu.Remove(this.tssPreProfileImportExport);
			this.tsMenu.Remove(this.tsmiProfileImportExport);
		}

		void tsmiProfileExport_Click(object sender, EventArgs e)
		{
			using(ProfileExportWindow pew = new ProfileExportWindow())
			{
				pew.ShowDialog();
			}
		}

		void tsmiProfileImport_Click(object sender, EventArgs e)
		{
			using(ProfileImportWindow piw = new ProfileImportWindow())
			{
				if(piw.ShowDialog() == DialogResult.OK)
				{
					PwProfile profile = piw.Profile;
					KeePass.Program.Config.PasswordGenerator.UserProfiles.Add(profile);
				}
			}
		}
	}
}