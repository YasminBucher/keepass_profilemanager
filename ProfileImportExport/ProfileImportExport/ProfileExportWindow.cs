﻿using KeePass.Util;
using KeePassLib.Cryptography.PasswordGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProfileImportExport
{
	public partial class ProfileExportWindow : Form
	{
		public ProfileExportWindow()
		{
			InitializeComponent();

			List<PwProfile> profiles = PwGeneratorUtil.GetAllProfiles(true);

			this.cmbProfileSelection.DisplayMember = "Name";
			this.cmbProfileSelection.DataSource = profiles;
		}

		private void btnExport_Click(object sender, EventArgs e)
		{
			PwProfile profile = (PwProfile)this.cmbProfileSelection.SelectedItem;
			this.SaveProfile(profile);

			this.DialogResult = DialogResult.OK;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void btnSelectProfileSaveFile_Click(object sender, EventArgs e)
		{
			using(SaveFileDialog sfd = new SaveFileDialog())
			{
				sfd.Filter = string.Format("KeePass Profil Datei (*{0})|*{0}", Const.PROFILE_FILE_EXT);
				sfd.AddExtension = true;
				sfd.OverwritePrompt = true;
				sfd.ValidateNames = true;

				if(sfd.ShowDialog() == DialogResult.OK)
				{
					this.txtProfileSaveFile.Text = sfd.FileName;
				}
			}
		}

		private void SaveProfile(PwProfile profile)
		{
			XmlSerializer ser = new XmlSerializer(typeof(PwProfile));

			using(FileStream fs = new FileStream(this.txtProfileSaveFile.Text, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using(StreamWriter sw = new StreamWriter(fs))
				{
					ser.Serialize(sw, profile);
				}
			}
		}
	}
}