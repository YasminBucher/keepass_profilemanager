﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProfileImportExport
{
	internal class Util
	{
		private static Version appVersion;

		static Util()
		{
			Version version = Assembly.GetExecutingAssembly().GetName().Version;
			appVersion = version;
		}

		private Util()
		{

		}

		public static Version GetAppVersion()
		{
			return appVersion;
		}
	}
}
