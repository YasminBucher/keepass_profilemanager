﻿namespace ProfileImportExport
{
	partial class ProfileImportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblProfileFile = new System.Windows.Forms.Label();
			this.btnSelectProfileFile = new System.Windows.Forms.Button();
			this.txtProfileName = new System.Windows.Forms.TextBox();
			this.btnImport = new System.Windows.Forms.Button();
			this.txtProfileFile = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblProfileName = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblProfileFile
			// 
			this.lblProfileFile.AutoSize = true;
			this.lblProfileFile.Location = new System.Drawing.Point(12, 9);
			this.lblProfileFile.Name = "lblProfileFile";
			this.lblProfileFile.Size = new System.Drawing.Size(58, 13);
			this.lblProfileFile.TabIndex = 0;
			this.lblProfileFile.Text = "Profil-Datei";
			// 
			// btnSelectProfileFile
			// 
			this.btnSelectProfileFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelectProfileFile.Location = new System.Drawing.Point(416, 23);
			this.btnSelectProfileFile.Name = "btnSelectProfileFile";
			this.btnSelectProfileFile.Size = new System.Drawing.Size(26, 23);
			this.btnSelectProfileFile.TabIndex = 1;
			this.btnSelectProfileFile.Text = "...";
			this.btnSelectProfileFile.UseVisualStyleBackColor = true;
			this.btnSelectProfileFile.Click += new System.EventHandler(this.btnSelectProfileFile_Click);
			// 
			// txtProfileName
			// 
			this.txtProfileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtProfileName.Location = new System.Drawing.Point(12, 84);
			this.txtProfileName.Name = "txtProfileName";
			this.txtProfileName.Size = new System.Drawing.Size(398, 20);
			this.txtProfileName.TabIndex = 2;
			this.txtProfileName.TextChanged += new System.EventHandler(this.txtProfileName_TextChanged);
			// 
			// btnImport
			// 
			this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnImport.Location = new System.Drawing.Point(367, 117);
			this.btnImport.Name = "btnImport";
			this.btnImport.Size = new System.Drawing.Size(75, 23);
			this.btnImport.TabIndex = 3;
			this.btnImport.Text = "Importieren";
			this.btnImport.UseVisualStyleBackColor = true;
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			// 
			// txtProfileFile
			// 
			this.txtProfileFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtProfileFile.Location = new System.Drawing.Point(12, 25);
			this.txtProfileFile.Name = "txtProfileFile";
			this.txtProfileFile.ReadOnly = true;
			this.txtProfileFile.Size = new System.Drawing.Size(398, 20);
			this.txtProfileFile.TabIndex = 4;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Location = new System.Drawing.Point(286, 117);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Abbrechen";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblProfileName
			// 
			this.lblProfileName.AutoSize = true;
			this.lblProfileName.Location = new System.Drawing.Point(12, 68);
			this.lblProfileName.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.lblProfileName.Name = "lblProfileName";
			this.lblProfileName.Size = new System.Drawing.Size(61, 13);
			this.lblProfileName.TabIndex = 6;
			this.lblProfileName.Text = "Profil-Name";
			// 
			// ProfileImportWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(454, 152);
			this.Controls.Add(this.lblProfileName);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.txtProfileFile);
			this.Controls.Add(this.btnImport);
			this.Controls.Add(this.txtProfileName);
			this.Controls.Add(this.btnSelectProfileFile);
			this.Controls.Add(this.lblProfileFile);
			this.Name = "ProfileImportWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Profile Import";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblProfileFile;
		private System.Windows.Forms.Button btnSelectProfileFile;
		private System.Windows.Forms.TextBox txtProfileName;
		private System.Windows.Forms.Button btnImport;
		private System.Windows.Forms.TextBox txtProfileFile;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblProfileName;
	}
}