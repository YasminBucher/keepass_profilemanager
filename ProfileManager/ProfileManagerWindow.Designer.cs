﻿namespace ProfileManager
{
	partial class ProfileManagerWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgvProfiles = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.dgvProfiles)).BeginInit();
			this.SuspendLayout();
			// 
			// dgvProfiles
			// 
			this.dgvProfiles.AllowUserToAddRows = false;
			this.dgvProfiles.AllowUserToDeleteRows = false;
			this.dgvProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvProfiles.Location = new System.Drawing.Point(151, 149);
			this.dgvProfiles.Name = "dgvProfiles";
			this.dgvProfiles.ReadOnly = true;
			this.dgvProfiles.Size = new System.Drawing.Size(240, 150);
			this.dgvProfiles.TabIndex = 0;
			// 
			// ProfileManagerWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(804, 494);
			this.Controls.Add(this.dgvProfiles);
			this.Name = "ProfileManagerWindow";
			this.Text = "Profile Manager";
			this.Load += new System.EventHandler(this.ProfileManagerWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvProfiles)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvProfiles;
	}
}