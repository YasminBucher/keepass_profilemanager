﻿using KeePass.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProfileManager
{
    public class ProfileManagerExt : Plugin
    {
		private IPluginHost pluginHost = null;
		private ToolStripItemCollection tsMenu = null;
		private ToolStripSeparator tssPreProfileManager = null;
		private ToolStripMenuItem tsmiProfileManager = null;

		public override bool Initialize(IPluginHost host)
		{
			this.pluginHost = host;

			this.tsMenu = this.pluginHost.MainWindow.ToolsMenu.DropDownItems;

			this.tssPreProfileManager = new ToolStripSeparator();
			this.tsMenu.Add(this.tssPreProfileManager);

			this.tsmiProfileManager = new ToolStripMenuItem("Profile Manager");
			this.tsmiProfileManager.Click += tsmiProfileManager_Click;
			this.tsMenu.Add(this.tsmiProfileManager);

			return true;
		}

		public override void Terminate()
		{
			this.tsMenu.Remove(this.tssPreProfileManager);
			this.tsMenu.Remove(this.tsmiProfileManager);
			this.tsmiProfileManager.Click -= tsmiProfileManager_Click;
		}

		void tsmiProfileManager_Click(object sender, EventArgs e)
		{
			using(ProfileManagerWindow pmw = new ProfileManagerWindow())
			{
				pmw.ShowDialog(this.pluginHost.MainWindow);
			}
		}
    }
}